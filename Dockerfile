FROM bellsoft/liberica-openjdk-debian:17
ADD server/build/libs/server.jar .

EXPOSE 443
EXPOSE 8080
EXPOSE 5005
CMD java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005 -jar server.jar