package ru.javadyori.importunet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import ru.javadyori.importunet.models.entyties.user.UserInfo;
import ru.javadyori.importunet.repositories.users.UserRepository;
import ru.javadyori.importunet.services.credentials.RolesService;

import java.util.Optional;
import java.util.Set;

@SpringBootApplication
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ImportunetApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(ImportunetApplication.class, args);

    }


}

