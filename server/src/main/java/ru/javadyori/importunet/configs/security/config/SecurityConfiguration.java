package ru.javadyori.importunet.configs.security.config;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;
import ru.javadyori.importunet.configs.security.jwt.JwtCsrfFilter;
import ru.javadyori.importunet.configs.security.jwt.JwtTokenRepository;

@Configuration
@FieldDefaults(level = AccessLevel.PRIVATE)
@EnableWebSecurity
public class SecurityConfiguration {

    final UserDetailsService userDetailsService;
    final JwtTokenRepository repository;
    final HandlerExceptionResolver resolver;

    public SecurityConfiguration(@Qualifier("postgresUserDetailService") UserDetailsService userDetailsService,
                                 JwtTokenRepository repository,
                                 @Autowired @Qualifier("handlerExceptionResolver") HandlerExceptionResolver resolver) {
        this.userDetailsService = userDetailsService;
        this.repository = repository;
        this.resolver = resolver;
    }

    @Bean
    @Primary
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf().disable()
//                .addFilterAt(new JwtCsrfFilter(repository, resolver), CsrfFilter.class)
                .authorizeHttpRequests((authz) -> authz
                        .antMatchers("/api/rest/test/greeting",
                                "/api/rest/auth/signUp").permitAll()
                        .antMatchers("/api/rest/test/**", "/api/rest/auth/**").authenticated()
                )
                .userDetailsService(userDetailsService)
                .httpBasic()
                .authenticationEntryPoint((request, response, authException) -> resolver.resolveException(request, response, null, authException));
        return http.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
