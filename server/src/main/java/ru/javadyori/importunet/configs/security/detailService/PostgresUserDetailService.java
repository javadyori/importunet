package ru.javadyori.importunet.configs.security.detailService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.javadyori.importunet.services.credentials.UserCredentialService;

import static java.lang.String.format;

@Component
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Slf4j
public class PostgresUserDetailService implements UserDetailsService {

    final UserCredentialService userCredentialService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return userCredentialService.findUserByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException(format("Can't find user with username: %s", email)));
    }
}
