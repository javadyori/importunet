package ru.javadyori.importunet.configs.security.jwt;

import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.vavr.control.Try;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.InvalidCsrfTokenException;
import org.springframework.security.web.csrf.MissingCsrfTokenException;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

import static java.util.Objects.isNull;
import static org.apache.logging.log4j.util.Strings.isBlank;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Slf4j
public class JwtCsrfFilter extends OncePerRequestFilter {

    CsrfTokenRepository tokenRepository;
    HandlerExceptionResolver resolver;

    public JwtCsrfFilter(CsrfTokenRepository tokenRepository,  HandlerExceptionResolver resolver) {
        this.tokenRepository = tokenRepository;
        this.resolver = resolver;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) {
        request.setAttribute(HttpServletResponse.class.getName(), response);

        CsrfToken token = Optional.ofNullable(this.tokenRepository.loadToken(request)).orElseGet(() -> {
            final CsrfToken csrfToken = this.tokenRepository.generateToken(request);
            this.tokenRepository.saveToken(csrfToken, request, response);
            return csrfToken;
        });

        request.setAttribute(CsrfToken.class.getName(), token);
        request.setAttribute(token.getParameterName(), token);

        if (request.getServletPath().equals("/api/rest/auth/signIn")
                || request.getServletPath().equals("/api/rest/auth/signUp")) {
            Try.run(() -> filterChain.doFilter(request, response))
                    .onFailure(e -> resolveException(request, response, null, new MissingCsrfTokenException(token.getToken())));
        } else {
            final String actualToken = Optional.ofNullable(request.getHeader(token.getHeaderName()))
                    .orElseGet(() -> request.getParameter(token.getParameterName()));

            Try.run(() -> {
                if (isBlank(actualToken)) {
                    resolveException(request, response, null, new InvalidCsrfTokenException(token, actualToken));
                } else {
                    Jwts.parser()
                            .setSigningKey(((JwtTokenRepository)tokenRepository).getSecret())
                            .parseClaimsJws(actualToken);
                    filterChain.doFilter(request, response);
                }
            }).onFailure(JwtException.class, e -> {
                log.info("Invalid CSRF token found for " + UrlUtils.buildFullRequestUrl(request));
                if (isNull(token)) {
                    resolveException(request, response, actualToken, new MissingCsrfTokenException(actualToken));
                } else {
                    resolveException(request, response, null, new InvalidCsrfTokenException(token, actualToken));
                }
            });
        }
    }

    private ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, String actualToken, Exception e) {
        return resolver.resolveException(request, response, actualToken, e);
    }
}
