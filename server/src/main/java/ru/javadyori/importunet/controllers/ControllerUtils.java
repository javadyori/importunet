package ru.javadyori.importunet.controllers;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.javadyori.importunet.models.dtos.user.UserInfoDto;
import ru.javadyori.importunet.services.users.UserService;

import javax.servlet.http.HttpSession;
import java.util.Optional;
import java.util.function.Function;

@Service
@AllArgsConstructor
public class ControllerUtils {
    final UserService userService;

    public <T> ResponseEntity<T> checkUserAndDo(HttpSession session, Function<UserInfoDto, ResponseEntity<T>> function) {
        return Optional.ofNullable(session.getAttribute("username"))
                .filter(String.class::isInstance)
                .map(String.class::cast)
                .flatMap(userService::getUserByUsername)
                .map(function)
                .orElse(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }
}
