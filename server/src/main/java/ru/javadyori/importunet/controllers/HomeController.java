package ru.javadyori.importunet.controllers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/rest/home")
public class HomeController {


    @GetMapping
    @PreAuthorize("hasAnyRole({'USER', 'ADMIN'})")
    public ResponseEntity<?> home(){
        return ResponseEntity.ok("home");
    }
}
