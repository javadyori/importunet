package ru.javadyori.importunet.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.javadyori.importunet.mappers.user.UserMapper;
import ru.javadyori.importunet.models.dtos.orgs.LoginDto;
import ru.javadyori.importunet.models.dtos.user.UserInfoDto;
import ru.javadyori.importunet.services.users.UserService;

import javax.servlet.http.HttpSession;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Optional;

@RestController
@RequestMapping("api/rest/auth")
@RequiredArgsConstructor
public class LoginController {

    final UserService userService;
    final UserMapper userMapper;


    @GetMapping("signIn")
    public ResponseEntity<?> signIn(HttpSession session, @RequestHeader("Authorization") String basic) {
        getUserName(basic).ifPresent(login -> session.setAttribute("username", login));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Location", "/home.html");
        return new ResponseEntity<>(headers, HttpStatus.FOUND);
    }

    @PostMapping("signUp")
    public ResponseEntity<UserInfoDto> signUp(@RequestBody LoginDto dto, HttpSession session) {
        UserInfoDto user = userService.createUser(dto);
        session.setAttribute("username", user.username());

        return ResponseEntity.ok(user);
    }

    private Optional<String> getUserName(String basic) {
        return Optional.ofNullable(basic)
                .map(b -> b.substring("Basic ".length()).getBytes(StandardCharsets.UTF_8))
                .map(Base64.getDecoder()::decode)
                .map(String::new)
                .map(b -> b.split(":")[0]);
    }
}
