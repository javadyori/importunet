package ru.javadyori.importunet.controllers;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.javadyori.importunet.mappers.orgs.OrganizationMapper;
import ru.javadyori.importunet.models.dtos.orgs.CreateOrganizationDto;
import ru.javadyori.importunet.models.dtos.user.UpdateUserInfoDto;
import ru.javadyori.importunet.services.organizations.OrganizationService;
import ru.javadyori.importunet.services.users.UserService;

import javax.servlet.http.HttpSession;

@RestController
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Slf4j
@RequestMapping("api/rest/org/")
public class OrganizationController {

    final OrganizationService organizationService;
    final UserService userService;
    final ControllerUtils controllerUtils;
    final OrganizationMapper organizationMapper;

    @PostMapping
    public ResponseEntity<?> createOrganization(@RequestBody CreateOrganizationDto createOrganizationDto, HttpSession session) {
        return controllerUtils.checkUserAndDo(session, userInfoDto -> {
            try {
                var organization = organizationService.addOrganization(createOrganizationDto);
                userService.updateUser(UpdateUserInfoDto.byUserInfo(userInfoDto)
                        .withOrganization(organizationMapper.toEntity(organization))
                );
                return ResponseEntity.ok(organization);
            } catch (Exception e) {
                log.error("Error:", e);
                return ResponseEntity.internalServerError().body(e.getMessage());
            }
        });
    }

    @PutMapping
    public ResponseEntity<?> updateOrganization(@RequestBody CreateOrganizationDto dto) {
        try {
            return ResponseEntity.ok(organizationService.updateOrganization(dto));
        } catch (Exception e) {
            log.error("Error:", e);
            return ResponseEntity.internalServerError().body(e.getMessage());
        }
    }
}
