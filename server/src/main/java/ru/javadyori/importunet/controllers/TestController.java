package ru.javadyori.importunet.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("api/rest/test")
public class TestController {

    @GetMapping("greeting")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<String> greeting() {
        return ResponseEntity.ok("Hello");
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> notAvailableEndpoint(HttpServletRequest request) {
        return ResponseEntity.ok("Strnge");
    }
}
