package ru.javadyori.importunet.controllers;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.javadyori.importunet.models.dtos.user.UpdateUserInfoDto;
import ru.javadyori.importunet.models.dtos.user.UserInfoDto;
import ru.javadyori.importunet.services.users.UserService;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.apache.logging.log4j.util.Strings.isNotBlank;

@RestController
@RequestMapping("api/rest/user")
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserController {

    final UserService userService;

    @PutMapping()
    public ResponseEntity<?> updateUser(@RequestBody UpdateUserInfoDto dto, HttpSession session) {
        log.info("Получен запрос на обновление пользователя");
        try {
            final String username = (String) session.getAttribute("username");
            if (username == null) {
                return (ResponseEntity<?>) ResponseEntity.status(HttpStatus.UNAUTHORIZED);
            }

            var newDto = new UpdateUserInfoDto(dto.userId(), username, dto.email(), dto.phone(), dto.firstName(),
                    dto.secondName(), dto.middleName(), dto.birthdayDate(), LocalDateTime.now(), Set.of("ROLE_USER"),
                    dto.organization());
            return ResponseEntity.ok(userService.updateUser(newDto));
        } catch (Exception e) {
            log.error("Ошибка в процессе обновления пользователя: ", e);
            return ResponseEntity.internalServerError().body(e.getMessage());
        }
    }


    @GetMapping
    public ResponseEntity<?> getById(@RequestParam(name = "userId", defaultValue = "0") long id,
                                     @RequestParam(name = "firstName", defaultValue = "") String firstName,
                                     @RequestParam(name = "secondName", defaultValue = "") String secondName) {

        log.info("Получен запрос на получение пользователей с параметрами: " +
                "id = {}, firstName = {}, secondName = {}", id, firstName, secondName);
        if (id != 0) {
            Optional<UserInfoDto> userBuId = userService.getUserBuId(id);
            if (userBuId.isPresent()) {
                return ResponseEntity.ok(userBuId.get());
            } else return ResponseEntity.ok("Пользователь не найден");
        }

        if (isNotBlank(firstName)) {
            List<UserInfoDto> allUsersByName = userService.getAllUsersByName(firstName);
            if (isNotBlank(secondName)) {
                allUsersByName = allUsersByName.stream()
                        .filter((user) -> user.secondName().equals(secondName)).collect(Collectors.toList());
            }
            return ResponseEntity.ok(allUsersByName);
        }

        if (isNotBlank(secondName)) return ResponseEntity.ok(userService.getAllUsersBySecondName(secondName));

        return ResponseEntity.ok(userService.getAllUsers());
    }

    @GetMapping("whoami")
    public ResponseEntity<?> whoAmI(HttpSession session) {
        return Optional.ofNullable(session.getAttribute("username"))
                .filter(String.class::isInstance)
                .map(String.class::cast)
                .flatMap(userService::getUserByUsername)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }

}
