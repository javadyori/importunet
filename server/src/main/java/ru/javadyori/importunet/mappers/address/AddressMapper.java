package ru.javadyori.importunet.mappers.address;

import ru.javadyori.importunet.models.dtos.address.AddressDto;
import ru.javadyori.importunet.models.entyties.address.Address;

public interface AddressMapper {

    AddressDto toDto(Address address);
    Address toEntity(AddressDto dto) throws IllegalArgumentException;
}
