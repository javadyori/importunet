package ru.javadyori.importunet.mappers.address;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;
import ru.javadyori.importunet.models.dtos.address.AddressDto;
import ru.javadyori.importunet.models.entyties.address.Address;

@Component
@RequiredArgsConstructor

@FieldDefaults(level = AccessLevel.PRIVATE)
public class AddressMapperImpl implements AddressMapper {


    @Override
    public AddressDto toDto(Address address) {
        return AddressDto.builder()
                .id(address.getId())
                .city(address.getCity())
                .house(address.getHouse())
                .officeRoom(address.getOfficeRoom())
                .addressType(address.getAddressType())
                .street(address.getStreet())
                .organizationId(address.getOrganization())
                .build();
    }

    @Override
    public Address toEntity(AddressDto dto) {
        return new Address()
                .setHouse(dto.house());
    }
}
