package ru.javadyori.importunet.mappers.orgs;

import ru.javadyori.importunet.models.dtos.orgs.CreateOrganizationDto;
import ru.javadyori.importunet.models.dtos.orgs.OrganizationDto;
import ru.javadyori.importunet.models.entyties.orgs.Organization;

public interface OrganizationMapper {

    Organization toEntity(CreateOrganizationDto dto) throws IllegalArgumentException;
    Organization toEntity(OrganizationDto dto);
    OrganizationDto toDto(Organization organization);
}
