package ru.javadyori.importunet.mappers.orgs;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;
import ru.javadyori.importunet.mappers.address.AddressMapper;
import ru.javadyori.importunet.models.dtos.orgs.CreateOrganizationDto;
import ru.javadyori.importunet.models.dtos.orgs.OrganizationDto;
import ru.javadyori.importunet.models.entyties.orgs.Organization;
import ru.javadyori.importunet.services.address.AddressService;
import ru.javadyori.importunet.services.organizations.OrganizationTypeService;
import ru.javadyori.importunet.services.organizations.OwnershipTypeService;

@Component
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrganizationMapperImpl implements OrganizationMapper {

    final AddressMapper addressMapper;
    final OrganizationTypeService organizationTypeService;
    final OwnershipTypeService ownershipTypeService;
    final OwnershipTypeMapper ownershipTypeMapper;
    final AddressService addressService;

    @Override
    public Organization toEntity(CreateOrganizationDto dto) throws IllegalArgumentException {
        return new Organization()
                .setId(dto.id())
                .setName(dto.name())
                .setInnNumber(dto.innNumber())
                .setPhone(dto.phone())
                .setWebsite(dto.website())
                .setDescription(dto.description());
    }

    @Override
    public Organization toEntity(OrganizationDto dto) {
        return new Organization()
                .setId(dto.id())
                .setName(dto.name())
                .setInnNumber(dto.innNumber())
                .setPhone(dto.phone())
                .setWebsite(dto.website())
                .setDescription(dto.description());
    }

    @Override
    public OrganizationDto toDto(Organization organization) {

        return OrganizationDto.builder()
                .id(organization.getId())
                .name(organization.getName())
                .address(organization.getAddress() == null ? null : addressMapper.toDto(organization.getAddress()))
                .creationDate(organization.getCreationDate())
                .lastUpdate(organization.getLastUpdate())
                .innNumber(organization.getInnNumber())
                .description(organization.getDescription())
                .ownershipType(organization.getOwnershipType() == null ? null : ownershipTypeMapper.toDto(organization.getOwnershipType()))
                .phone(organization.getPhone())
                .website(organization.getWebsite())
                .organizationType(organization.getOrganizationType())
                .build();
    }
}
