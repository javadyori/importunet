package ru.javadyori.importunet.mappers.orgs;

import ru.javadyori.importunet.models.dtos.orgs.OwnershipTypeDto;
import ru.javadyori.importunet.models.entyties.orgs.OwnershipType;

public interface OwnershipTypeMapper {

    OwnershipTypeDto toDto(OwnershipType ownershipType);
}
