package ru.javadyori.importunet.mappers.orgs;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;
import ru.javadyori.importunet.models.dtos.orgs.OwnershipTypeDto;
import ru.javadyori.importunet.models.entyties.orgs.OwnershipType;

@Component
@RequiredArgsConstructor
@FieldDefaults(level= AccessLevel.PRIVATE)
public class OwnershipTypeMapperImpl implements OwnershipTypeMapper {
    @Override
    public OwnershipTypeDto toDto(OwnershipType ownershipType) {
        return new OwnershipTypeDto(ownershipType.getName(), ownershipType.getFnsCode());
    }
}
