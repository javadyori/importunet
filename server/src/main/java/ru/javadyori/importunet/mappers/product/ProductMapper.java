package ru.javadyori.importunet.mappers.product;

import org.mapstruct.Mapper;
import ru.javadyori.importunet.models.dtos.product.*;
import ru.javadyori.importunet.models.entyties.product.*;

@Mapper(componentModel = "spring")
public abstract class ProductMapper {

    //Основная мапеер продукта
    public abstract ProductDto toDto(Product product);

    public abstract Product toEntity(ProductDto dto);

    public abstract Product toEntity(CreateProductDto dto);

    //Маперы для зависимых сущностей
    public abstract ProductTag toEntity(ProductTag productTag);

    public abstract Product toDto(ProductTagDto productTagDto);

    public abstract OkvedDto toDto(Okved okved);

    public abstract Okved toEntity(OkvedDto dto);

    public abstract OKPDDto toDto(OKPD okpd);

    public abstract OKPD toEntity(OKPDDto dto);

    public abstract AnalogDto toDto(Analog analog);

    public abstract Analog toEntity(AnalogDto dto);

    public abstract AttacheDto toDto(Attache attache);

    public abstract Attache toEntity(AttacheDto attacheDto);
}
