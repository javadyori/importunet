package ru.javadyori.importunet.mappers.user;

import ru.javadyori.importunet.models.dtos.user.UpdateUserInfoDto;
import ru.javadyori.importunet.models.dtos.user.UserInfoDto;
import ru.javadyori.importunet.models.entyties.user.UserInfo;

import java.util.Collection;

public interface UserMapper {

    UserInfoDto toDto(UserInfo userInfo);
    Collection<UserInfoDto> toDto(Collection<UserInfo> collection);
    UserInfo toEntity(UpdateUserInfoDto updateUserInfoDto) throws IllegalArgumentException;
}
