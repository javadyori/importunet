package ru.javadyori.importunet.mappers.user;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;
import ru.javadyori.importunet.models.dtos.user.UpdateUserInfoDto;
import ru.javadyori.importunet.models.dtos.user.UserInfoDto;
import ru.javadyori.importunet.models.entyties.user.UserInfo;
import ru.javadyori.importunet.models.entyties.user.UserRole;
import ru.javadyori.importunet.services.credentials.RolesService;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserMapperImpl implements UserMapper {

    final RolesService rolesService;

    @Override
    public UserInfoDto toDto(UserInfo userInfo) {
        return UserInfoDto.builder()
                .userId(userInfo.getUserId())
                .username(userInfo.getUsername())
                .email(userInfo.getEmail())
                .firstName(userInfo.getFirstName())
                .secondName(userInfo.getSecondName())
                .middleName(userInfo.getMiddleName())
                .birthdayDate(userInfo.getBirthdayDate())
                .phone(userInfo.getPhone())
                .creationDate(userInfo.getCreationDate())
                .roles(userInfo.getRoles())
                .build();
    }

    @Override
    public Collection<UserInfoDto> toDto(Collection<UserInfo> collection) {
        return collection.stream().map(this::toDto).collect(Collectors.toList());
    }

    @Override
    public UserInfo toEntity(UpdateUserInfoDto updateUserInfoDto) throws IllegalArgumentException {
        return new UserInfo().setFirstName(updateUserInfoDto.firstName())
                .setSecondName(updateUserInfoDto.secondName())
                .setMiddleName(updateUserInfoDto.middleName())
                .setEmail(updateUserInfoDto.email())
                .setBirthdayDate(updateUserInfoDto.birthdayDate())
                .setPhone(updateUserInfoDto.phone())
                .setRoles(getRoles(updateUserInfoDto.roles()))
                .setOrganization(updateUserInfoDto.organization());
    }

    private Set<UserRole> getRoles(Set<String> roles) throws IllegalArgumentException {
        return roles.stream().map(rolesService::getByName).collect(Collectors.toSet());
    }
}
