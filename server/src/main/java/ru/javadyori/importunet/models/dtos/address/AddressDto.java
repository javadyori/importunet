package ru.javadyori.importunet.models.dtos.address;

import ru.javadyori.importunet.models.entyties.address.AddressType;
import ru.javadyori.importunet.models.entyties.orgs.Organization;
import ru.javadyori.importunet.models.entyties.address.Street;

public record AddressDto(long id, String city, int house, String officeRoom, String street, String addressType, long organizationId) {


    public static AddressDtoBuilder builder() {
        return new AddressDtoBuilder();
    }


    public static class AddressDtoBuilder {
        private long id;
        private String city;
        private int house;
        private String officeRoom;
        private String street;
        private String addressType;
        private long organizationId;

        AddressDtoBuilder() {
        }

        public AddressDtoBuilder id(long id) {
            this.id = id;
            return this;
        }

        public AddressDtoBuilder city(String city) {
            this.city = city;
            return this;
        }

        public AddressDtoBuilder house(int house) {
            this.house = house;
            return this;
        }

        public AddressDtoBuilder officeRoom(String officeRoom) {
            this.officeRoom = officeRoom;
            return this;
        }

        public AddressDtoBuilder street(Street street) {
            this.street = street.getName();
            return this;
        }

        public AddressDtoBuilder addressType(AddressType addressType) {
            this.addressType = addressType.getName().name();
            return this;
        }

        public AddressDtoBuilder organizationId(Organization organization) {
            this.organizationId = organization.getId();
            return this;
        }


        public AddressDto build() {
            return new AddressDto(id, city, house, officeRoom, street, addressType, organizationId);
        }

    }
}