package ru.javadyori.importunet.models.dtos.orgs;

import ru.javadyori.importunet.models.dtos.address.AddressDto;

import java.io.Serializable;

public record CreateOrganizationDto(long id,
                                    String name,
                                    String phone,
                                    String website,
                                    String innNumber,
                                    OwnershipTypeDto ownershipType,
                                    String organizationType,
                                    AddressDto address,
                                    String description) implements Serializable {
}
