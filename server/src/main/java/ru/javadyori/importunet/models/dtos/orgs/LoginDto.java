package ru.javadyori.importunet.models.dtos.orgs;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import org.springframework.security.crypto.bcrypt.BCrypt;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class LoginDto {
    String username;
    String email;
    String password;

    public String getPassword() {
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }
}
