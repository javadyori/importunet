package ru.javadyori.importunet.models.dtos.orgs;

import ru.javadyori.importunet.models.dtos.address.AddressDto;
import ru.javadyori.importunet.models.entyties.orgs.OrganizationType;

import java.time.LocalDateTime;


public record OrganizationDto (long id, String name, String phone, String website, String innNumber, String description, OwnershipTypeDto ownershipType, String organizationType, AddressDto address, LocalDateTime creationDate, LocalDateTime lastUpdate) {

    public static OrganizationDtoBuilder builder() {
        return new OrganizationDtoBuilder();
    }

    public static class OrganizationDtoBuilder {
        private long id;
        private String name;
        private String phone;
        private String website;
        private String innNumber;
        private OwnershipTypeDto ownershipType;
        private String organizationType;
        private String description;
        private AddressDto address;
        private LocalDateTime creationDate;
        private LocalDateTime lastUpdate;

        OrganizationDtoBuilder() {
        }

        public OrganizationDtoBuilder id(long id) {
            this.id = id;
            return this;
        }

        public OrganizationDtoBuilder name(String name) {
            this.name = name;
            return this;
        }

        public OrganizationDtoBuilder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public OrganizationDtoBuilder website(String website) {
            this.website = website;
            return this;
        }

        public OrganizationDtoBuilder innNumber(String innNumber) {
            this.innNumber = innNumber;
            return this;
        }

        public OrganizationDtoBuilder description(String description) {
            this.description = description;
            return this;
        }

        public OrganizationDtoBuilder ownershipType(OwnershipTypeDto ownershipType) {
            this.ownershipType = ownershipType;
            return this;
        }

        public OrganizationDtoBuilder organizationType(OrganizationType organizationType) {
            this.organizationType = organizationType == null ? null : organizationType.getName().name();
            return this;
        }

        public OrganizationDtoBuilder address(AddressDto address) {
            this.address = address;
            return this;
        }

        public OrganizationDtoBuilder creationDate(LocalDateTime creationDate) {
            this.creationDate = creationDate;
            return this;
        }

        public OrganizationDtoBuilder lastUpdate(LocalDateTime lastUpdate) {
            this.lastUpdate = lastUpdate;
            return this;
        }

        public OrganizationDto build() {
            return new OrganizationDto(id, name, phone, website, innNumber, description, ownershipType, organizationType, address, creationDate, lastUpdate);
        }

    }
}
