package ru.javadyori.importunet.models.dtos.orgs;

import java.io.Serializable;

public record OwnershipTypeDto(String name, String fnsCode) implements Serializable {
}
