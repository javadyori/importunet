package ru.javadyori.importunet.models.dtos.product;

import java.io.Serializable;
import java.util.List;

public record AnalogDto(long id, String name, List<OKPDDto> okpds) implements Serializable {
}
