package ru.javadyori.importunet.models.dtos.product;

import java.io.Serializable;

public record AttacheDto(long id, byte[] data) implements Serializable {
}
