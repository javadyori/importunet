package ru.javadyori.importunet.models.dtos.product;

import java.io.Serializable;
import java.math.BigDecimal;

public record CreateProductDto(String name, String unit, BigDecimal price, String description, long organizationId) implements Serializable {
}
