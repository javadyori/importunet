package ru.javadyori.importunet.models.dtos.product;

import ru.javadyori.importunet.models.entyties.product.OKPD;

import java.io.Serializable;
import java.util.List;

public record OKPDDto(long id, String name) implements Serializable {
}
