package ru.javadyori.importunet.models.dtos.product;

import ru.javadyori.importunet.models.entyties.product.Product;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public record OkvedDto(long id, String name) implements Serializable {
}
