package ru.javadyori.importunet.models.dtos.product;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public record ProductDto(long id, String name, String unit, BigDecimal price, String description, List<AttacheDto> attache,
                         List<OkvedDto> okveds, List<AnalogDto> analogs, List<ProductTagDto> tags,
                         LocalDateTime creationDate, LocalDateTime lastUpdateDate) implements Serializable {
}
