package ru.javadyori.importunet.models.dtos.product;

import java.io.Serializable;

public record ProductTagDto(long id, String name) implements Serializable {
}
