package ru.javadyori.importunet.models.dtos.user;

import ru.javadyori.importunet.models.entyties.orgs.Organization;
import ru.javadyori.importunet.models.entyties.user.UserInfo;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

public record UpdateUserInfoDto(long userId,
                                String username,
                                String email,
                                String phone,
                                String firstName,
                                String secondName,
                                String middleName,
                                LocalDate birthdayDate,
                                LocalDateTime creationDate,
                                Set<String> roles,
                                Organization organization) implements Serializable {
    public UpdateUserInfoDto withOrganization(Organization organization) {
        return new UpdateUserInfoDto(
                userId(),
                username(),
                email(),
                phone(),
                firstName(),
                secondName(),
                middleName(),
                birthdayDate(),
                creationDate(),
                roles(),
                organization
        );
    }

    public static UpdateUserInfoDto byUserInfo(UserInfoDto userInfoDto) {
        return new UpdateUserInfoDto(
                userInfoDto.userId(),
                userInfoDto.username(),
                userInfoDto.email(),
                userInfoDto.phone(),
                userInfoDto.firstName(),
                userInfoDto.secondName(),
                userInfoDto.middleName(),
                userInfoDto.birthdayDate(),
                userInfoDto.creationDate(),
                userInfoDto.roles(),
                null
        );
    }
}
