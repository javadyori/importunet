package ru.javadyori.importunet.models.dtos.user;

import ru.javadyori.importunet.models.entyties.user.UserRole;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

public record UserInfoDto(long userId, String username, String email, String phone, String firstName,
                          String secondName, String middleName, LocalDate birthdayDate,
                          LocalDateTime creationDate, Set<String> roles, Long organizationId) implements Serializable {


    public static UserInfoDtoBuilder builder(){
        return new UserInfoDtoBuilder();
    }

    public static class UserInfoDtoBuilder {
        private long userId;
        private String username;
        private String email;
        private String phone;
        private String firstName;
        private String secondName;
        private String middleName;
        private LocalDate birthdayDate;
        private LocalDateTime creationDate;
        private Set<String> roles;
        private Long organizationId;

        public UserInfoDtoBuilder userId(long userId) {
            this.userId = userId;
            return this;
        }

        public UserInfoDtoBuilder username(String username) {
            this.username = username;
            return this;
        }

        public UserInfoDtoBuilder email(String email) {
            this.email = email;
            return this;
        }

        public UserInfoDtoBuilder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public UserInfoDtoBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }
        public UserInfoDtoBuilder secondName(String secondName) {
            this.secondName = secondName;
            return this;
        }


        public UserInfoDtoBuilder middleName(String middleName) {
            this.middleName = middleName;
            return this;
        }

        public UserInfoDtoBuilder birthdayDate(LocalDate birthdayDate) {
            this.birthdayDate = birthdayDate;
            return this;
        }

        public UserInfoDtoBuilder creationDate(LocalDateTime creationDate) {
            this.creationDate = creationDate;
            return this;
        }

        public UserInfoDtoBuilder roles(Set<UserRole> roles){
            this.roles = roles.stream().map(role -> role.getRole()).collect(Collectors.toSet());
            return this;
        }

        public UserInfoDtoBuilder setOrganizationId(Long organizationId) {
            this.organizationId = organizationId;
            return this;
        }

        public UserInfoDto build() {
            return new UserInfoDto(userId, username, email, phone, firstName, secondName, middleName, birthdayDate, creationDate, roles, organizationId);
        }
    }
}
