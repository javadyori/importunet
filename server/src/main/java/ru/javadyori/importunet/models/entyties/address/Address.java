package ru.javadyori.importunet.models.entyties.address;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import ru.javadyori.importunet.models.entyties.orgs.Organization;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Getter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    long id;
    String city;
    int house;
    String officeRoom;
    @OneToOne
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    Organization organization;
    @ManyToOne
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @JoinColumn(name = "street_id")
    Street street;
    @ManyToOne
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @JoinColumn(name = "address_type_id")
    AddressType addressType;
    @CreationTimestamp
    LocalDateTime creationDate;
    @UpdateTimestamp
    LocalDateTime lastUpdate;

    public Address removeAddressType() {
        addressType.getAddresses().remove(this);
        addressType = null;
        return this;
    }

    public Address removeStreet() {
        street.getAddresses().remove(this);
        street = null;
        return this;
    }

    public Address addAddressType(AddressType addressType) {
        this.addressType = addressType;
        return this;
    }

    public Address setOrganization(Organization organization) {
        this.organization = organization;
        return this;
    }

    public Address addStreet(Street street) {
        this.street = street;
        return this;
    }

    public Address setCity(String city) {
        this.city = city;
        return this;
    }

    public Address setHouse(int house) {
        this.house = house;
        return this;
    }

    public Address setStreet(Street street) {
        this.street = street;
        return this;
    }

    public Address setAddressType(AddressType addressType) {
        this.addressType = addressType;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return id == address.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
