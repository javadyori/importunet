package ru.javadyori.importunet.models.entyties.address;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import ru.javadyori.importunet.models.entyties.AddressName;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AddressType {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    long id;
    @Enumerated(EnumType.STRING)
    AddressName name;
    @CreationTimestamp
    LocalDateTime creationDate;
    @UpdateTimestamp
    LocalDateTime lastUpdate;

    @OneToMany(fetch = FetchType.EAGER)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    Set<Address> addresses = new HashSet<>();

    public AddressType(AddressName addressName){
        this.name = addressName;
    }

    public AddressType setName(AddressName name) {
        this.name = name;
        return this;
    }

    public AddressType setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
        return this;
    }
}