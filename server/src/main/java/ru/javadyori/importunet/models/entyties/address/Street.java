package ru.javadyori.importunet.models.entyties.address;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "kladr")
public class Street {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    long id;
    String name;
    @OneToMany
            @Cascade(org.hibernate.annotations.CascadeType.ALL)
    Set<Address> addresses = new HashSet<>();
    @CreationTimestamp
    LocalDateTime creationDate;
    @UpdateTimestamp
    LocalDateTime lastUpdate;

    public Street(String name){
        this.name = name;
    }

    public Street setName(String name) {
        this.name = name;
        return this;
    }

    public Street setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
        return this;
    }
}
