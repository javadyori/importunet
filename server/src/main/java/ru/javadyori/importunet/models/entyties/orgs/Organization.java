package ru.javadyori.importunet.models.entyties.orgs;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import ru.javadyori.importunet.models.entyties.address.Address;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Organization {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    long id;
    String name;
    String phone;
    String website;
    @Column(unique = true)
    String innNumber;
    @Column(length = 600)
    String description;
    @ManyToOne
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @JoinColumn(name = "ownership_type_id")
    OwnershipType ownershipType;
    @ManyToOne
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @JoinColumn(name = "organization_type_id")
    OrganizationType organizationType;
    @OneToOne(mappedBy = "organization")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    Address address;
    @CreationTimestamp
    LocalDateTime creationDate;
    @UpdateTimestamp
    LocalDateTime lastUpdate;

    public Organization addOwnershipType(OwnershipType ownershipType){
        this.ownershipType = ownershipType;
        ownershipType.getOrganizations().add(this);
        return this;
    }

    public Organization setId(long id) {
        this.id = id;
        return this;
    }

    public Organization removeOwnershipType(){
        ownershipType.getOrganizations().remove(this);
        ownershipType = null;
        return this;
    }

    public Organization setDescription(String description) {
        this.description = description;
        return this;
    }

    public Organization addOrganizationType(OrganizationType organizationType){
        this.organizationType = organizationType;
        organizationType.getOrganizations().add(this);
        return this;
    }

    public Organization removeOrganizationType(){
        organizationType.getOrganizations().remove(this);
        organizationType = null;
        return this;
    }

    public Organization addAddress(Address address){
        this.address = address;
        address.setOrganization(this);
        return this;
    }

    public Organization removeAddress(){
        address.setOrganization(null);
        address = null;
        return this;
    }

    public Organization setName(String name) {
        this.name = name;
        return this;
    }

    public Organization setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public Organization setWebsite(String website) {
        this.website = website;
        return this;
    }

    public Organization setInnNumber(String innNumber) {
        this.innNumber = innNumber;
        return this;
    }

    public Organization setOwnershipType(OwnershipType ownershipType) {
        this.ownershipType = ownershipType;
        return this;
    }

    public Organization setOrganizationType(OrganizationType organizationType) {
        this.organizationType = organizationType;
        return this;
    }

    public Organization setAddress(Address address) {
        this.address = address;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Organization that = (Organization) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
