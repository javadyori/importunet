package ru.javadyori.importunet.models.entyties.orgs;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrganizationType {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    long id;
    @Enumerated(EnumType.STRING)
    OrganizationTypeName name;
    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    Set<Organization> organizations = new HashSet<>();
    @CreationTimestamp
    LocalDateTime creationDate;
    @UpdateTimestamp
    LocalDateTime lastUpdate;

    public OrganizationType(OrganizationTypeName name){
        this.name = name;
    }

    public OrganizationType setName(OrganizationTypeName name) {
        this.name = name;
        return this;
    }

    public OrganizationType setOrganizations(Set<Organization> organizations) {
        this.organizations = organizations;
        return this;
    }
}
