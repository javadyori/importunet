package ru.javadyori.importunet.models.entyties.orgs;

public enum OrganizationTypeName {
    OOO, IP, SELFSIGNED
}
