package ru.javadyori.importunet.models.entyties.orgs;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@ToString
@Table(name = "ownership_type")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OwnershipType {
    @Id
    @Column(nullable = false,unique = true)
    int id;
    @Column(nullable = false,unique = true)
    String name;
    String fnsCode;
    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    Set<Organization> organizations = new HashSet<>();
    @CreationTimestamp
    LocalDateTime creationDate;
    @UpdateTimestamp
    LocalDateTime lastUpdate;


    public OwnershipType setName(String name) {
        this.name = name;
        return this;
    }

    public OwnershipType setFnsCode(String fnsCode) {
        this.fnsCode = fnsCode;
        return this;
    }

    public OwnershipType setOrganizations(Set<Organization> organizations) {
        this.organizations = organizations;
        return this;
    }
}
