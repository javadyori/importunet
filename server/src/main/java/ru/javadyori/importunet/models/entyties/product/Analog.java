package ru.javadyori.importunet.models.entyties.product;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "analogs")
@Getter
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Analog {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    long id;
    String name;
    @ManyToMany(mappedBy = "analogs", fetch = FetchType.EAGER)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    List<OKPD> okpds = new ArrayList<>();
    @ManyToMany()
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    List<Product> products = new ArrayList<>();

    public Analog(String name){
        this.name = name;
    }

    public Analog setId(long id) {
        this.id = id;
        return this;
    }

    public Analog setName(String name) {
        this.name = name;
        return this;
    }

    public Analog setOkpds(List<OKPD> okpds) {
        this.okpds = okpds;
        return this;
    }

    public Analog setProducts(List<Product> products) {
        this.products = products;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Analog analog = (Analog) o;
        return id == analog.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
