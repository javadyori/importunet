package ru.javadyori.importunet.models.entyties.product;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "attaches")
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Attache {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    long id;
    byte[] data;
    @ManyToOne()
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    Product product;

    @CreationTimestamp
    LocalDateTime creationDate;
    @UpdateTimestamp
    LocalDateTime lastUpdateDate;

    public Attache setId(long id) {
        this.id = id;
        return this;
    }

    public Attache setData(byte[] data) {
        this.data = data;
        return this;
    }

    public Attache setProduct(Product product) {
        this.product = product;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Attache attache = (Attache) o;
        return id == attache.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
