package ru.javadyori.importunet.models.entyties.product;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "okved")
@Getter
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OKPD {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    long id;
    String name;
    @ManyToMany()
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    List<Analog> analogs = new ArrayList<>();

    public OKPD(String name){
        this.name = name;
    }

    public OKPD setId(long id) {
        this.id = id;
        return this;
    }

    public OKPD setName(String name) {
        this.name = name;
        return this;
    }

    public OKPD setAnalogs(List<Analog> analogs) {
        this.analogs = analogs;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OKPD okpd = (OKPD) o;
        return id == okpd.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
