package ru.javadyori.importunet.models.entyties.product;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class Okved {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    long id;
    String name;
    @ManyToMany(fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    List<Product> products = new ArrayList<>();

    public Okved(String name){
        this.name = name;
    }

    public Okved setId(long id) {
        this.id = id;
        return this;
    }

    public Okved setName(String name) {
        this.name = name;
        return this;
    }

    public Okved setProducts(List<Product> products) {
        this.products = products;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Okved okved = (Okved) o;
        return id == okved.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
