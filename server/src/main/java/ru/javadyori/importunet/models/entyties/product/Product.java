package ru.javadyori.importunet.models.entyties.product;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;
import ru.javadyori.importunet.models.entyties.orgs.Organization;

import javax.persistence.*;
import javax.persistence.Entity;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    long id;
    String name;
    String unit;
    BigDecimal price;;
    @Column(length = 600)
    String description;
    @OneToMany(mappedBy = "product")
    @LazyCollection(LazyCollectionOption.FALSE)
    @Cascade(CascadeType.ALL)
    List<Attache> attaches;
    @ManyToOne
    @Cascade(CascadeType.ALL)
    Organization organization;
    @ManyToMany(mappedBy = "products")
    @Cascade(CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    List<Okved> okveds = new ArrayList<>();
    @ManyToMany(mappedBy = "products")
    @LazyCollection(LazyCollectionOption.FALSE)
    @Cascade(CascadeType.ALL)
    List<Analog> analogs = new ArrayList<>();
    @ManyToMany(mappedBy = "products")
    @Cascade(CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    List<ProductTag> tags = new ArrayList<>();
    @CreationTimestamp
    LocalDateTime creationDate;
    @UpdateTimestamp
    LocalDateTime lastUpdateDate;

    public Product setId(long id) {
        this.id = id;
        return this;
    }

    public Product setName(String name) {
        this.name = name;
        return this;
    }

    public Product setUnit(String unit) {
        this.unit = unit;
        return this;
    }

    public Product setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public Product setDescription(String description) {
        this.description = description;
        return this;
    }

    public Product setAttaches(List<Attache> attache) {
        this.attaches = attache;
        return this;
    }

    public Product setOrganization(Organization organization) {
        this.organization = organization;
        return this;
    }

    public Product setOkveds(List<Okved> okveds) {
        this.okveds = okveds;
        return this;
    }

    public Product setAnalogs(List<Analog> analogs) {
        this.analogs = analogs;
        return this;
    }

    public Product setTags(List<ProductTag> tags) {
        this.tags = tags;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id == product.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
