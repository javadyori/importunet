package ru.javadyori.importunet.models.entyties.user;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.*;
import ru.javadyori.importunet.models.entyties.orgs.Organization;

import javax.persistence.*;
import javax.persistence.Entity;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    long userId;
    String username;
    @Column(unique = true, nullable = false)
    String email;
    @Column(nullable = false)
    String password;
    String phone;
    String firstName;
    String middleName;
    String secondName;
    LocalDate birthdayDate;
    @CreationTimestamp
    LocalDateTime creationDate;
    @UpdateTimestamp
    LocalDateTime lastUpdate;
    @ManyToMany
    @JoinTable(name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    @LazyCollection(LazyCollectionOption.FALSE)
    Set<UserRole> roles = new HashSet<>();

    @ManyToOne
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @JoinColumn(name = "id")
    @LazyCollection(LazyCollectionOption.FALSE)
    Organization organization;


    public UserInfo setRoles(Set<UserRole> roles) {
        this.roles = roles;
        return this;
    }

    public UserInfo addRole(UserRole role) {
        roles.add(role);
        role.addUser(this);
        return this;
    }


    public UserInfo setEmail(String email) {
        this.email = email;
        return this;
    }

    public UserInfo setUsername(String username) {
        this.username = username;
        return this;
    }

    public UserInfo setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public UserInfo setPassword(String password) {
        this.password = password;
        return this;
    }

    public UserInfo setPhone(String phoneNumber) {
        this.phone = phoneNumber;
        return this;
    }

    public UserInfo setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public UserInfo setSecondName(String secondName) {
        this.secondName = secondName;
        return this;
    }

    public UserInfo setBirthdayDate(LocalDate birthdayDate) {
        this.birthdayDate = birthdayDate;
        return this;
    }

    public UserInfo setOrganization(Organization organization) {
        this.organization = organization;
        return this;
    }
}
