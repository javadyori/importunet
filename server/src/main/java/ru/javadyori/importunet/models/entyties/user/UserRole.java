package ru.javadyori.importunet.models.entyties.user;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import ru.javadyori.importunet.models.Role;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class UserRole {

    public UserRole(Role role) {
        this.role = role;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long roleId;
    @Enumerated(EnumType.STRING)
    @Column(unique = true, nullable = false)
    Role role;
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "users_roles",
            joinColumns = @JoinColumn(name = "role_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    Set<UserInfo> userId = new HashSet<>();

    public Set<UserInfo> getUserId() {
        return userId;
    }

    public UserRole addUser(UserInfo userInfo) {
        userId.add(userInfo);
        return this;
    }

    public String getRole() {
        return role.name();
    }
}
