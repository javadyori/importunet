package ru.javadyori.importunet.repositories.address;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javadyori.importunet.models.entyties.address.Address;
import ru.javadyori.importunet.models.entyties.address.City;
import ru.javadyori.importunet.models.entyties.address.Street;

import java.util.Optional;

public interface AddressRepository extends JpaRepository<Address, Long> {
    Optional<Address> getAddressByCityAndStreetAndHouse(String city, Street street, int house);
}
