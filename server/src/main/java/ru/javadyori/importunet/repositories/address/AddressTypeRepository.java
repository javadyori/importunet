package ru.javadyori.importunet.repositories.address;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javadyori.importunet.models.entyties.AddressName;
import ru.javadyori.importunet.models.entyties.address.AddressType;

import java.util.Optional;

public interface AddressTypeRepository extends JpaRepository<AddressType, Long> {

    Optional<AddressType> findByName(AddressName name);
}
