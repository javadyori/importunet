package ru.javadyori.importunet.repositories.address;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javadyori.importunet.models.entyties.address.City;

import java.util.Optional;

public interface CityRepository extends JpaRepository<City, Long> {

    Optional<City> findByName(String city);
}
