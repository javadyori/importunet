package ru.javadyori.importunet.repositories.address;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javadyori.importunet.models.entyties.address.Street;

import java.util.Optional;

public interface StreetRepository extends JpaRepository<Street, Long> {

    Optional<Street> findByName(String name);
}
