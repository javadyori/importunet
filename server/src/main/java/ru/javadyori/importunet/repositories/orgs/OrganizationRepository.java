package ru.javadyori.importunet.repositories.orgs;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javadyori.importunet.models.entyties.orgs.Organization;

import java.util.Optional;

public interface OrganizationRepository extends JpaRepository<Organization, Long> {

    Optional<Organization> findByInnNumber(String innNumber);
}
