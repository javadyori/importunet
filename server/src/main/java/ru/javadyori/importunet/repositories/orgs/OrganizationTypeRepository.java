package ru.javadyori.importunet.repositories.orgs;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javadyori.importunet.models.entyties.orgs.OrganizationType;
import ru.javadyori.importunet.models.entyties.orgs.OrganizationTypeName;

import java.util.Optional;

public interface OrganizationTypeRepository extends JpaRepository<OrganizationType, Long>{

    Optional<OrganizationType> findByName(OrganizationTypeName type);
}
