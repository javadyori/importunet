package ru.javadyori.importunet.repositories.orgs;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javadyori.importunet.models.entyties.orgs.OwnershipType;

import java.util.Optional;

public interface OwnershipTypeRepository extends JpaRepository<OwnershipType, Long> {

    Optional<OwnershipType> findByNameAndFnsCode(String name, String fnsCode);
}
