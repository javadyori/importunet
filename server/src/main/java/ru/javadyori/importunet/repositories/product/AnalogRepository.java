package ru.javadyori.importunet.repositories.product;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javadyori.importunet.models.entyties.product.Analog;

public interface AnalogRepository extends JpaRepository<Analog, Long> {
}
