package ru.javadyori.importunet.repositories.product;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javadyori.importunet.models.entyties.product.Attache;

public interface AttacheRepository extends JpaRepository<Attache, Long> {
}
