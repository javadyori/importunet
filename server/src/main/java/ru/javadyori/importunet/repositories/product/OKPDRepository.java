package ru.javadyori.importunet.repositories.product;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javadyori.importunet.models.entyties.product.OKPD;

public interface OKPDRepository extends JpaRepository<OKPD, Long> {
}
