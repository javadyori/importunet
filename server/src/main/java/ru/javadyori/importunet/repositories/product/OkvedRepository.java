package ru.javadyori.importunet.repositories.product;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javadyori.importunet.models.entyties.product.Okved;

public interface OkvedRepository extends JpaRepository<Okved, Long> {
}
