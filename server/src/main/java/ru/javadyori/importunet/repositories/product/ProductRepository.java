package ru.javadyori.importunet.repositories.product;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.javadyori.importunet.models.entyties.product.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
