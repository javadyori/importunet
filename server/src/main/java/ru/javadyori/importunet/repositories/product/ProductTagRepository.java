package ru.javadyori.importunet.repositories.product;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javadyori.importunet.models.entyties.product.ProductTag;

public interface ProductTagRepository extends JpaRepository<ProductTag, Long> {
}
