package ru.javadyori.importunet.repositories.users;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javadyori.importunet.models.Role;
import ru.javadyori.importunet.models.entyties.user.UserRole;

import java.util.Optional;

public interface RolesRepository extends JpaRepository<UserRole, Long> {

    Optional<UserRole> findByRole(Role role);
}
