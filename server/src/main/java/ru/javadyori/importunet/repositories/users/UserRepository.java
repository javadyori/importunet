package ru.javadyori.importunet.repositories.users;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javadyori.importunet.models.entyties.user.UserInfo;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<UserInfo, Long> {

    Optional<UserInfo> findByUsername(String username);
    Optional<UserInfo> findByEmail(String email);
    List<UserInfo> findAllByFirstName(String name);
    List<UserInfo> findAllBySecondName(String name);
}
