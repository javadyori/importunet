package ru.javadyori.importunet.services.address;

import ru.javadyori.importunet.models.entyties.address.Address;
import ru.javadyori.importunet.models.entyties.address.Street;

public interface AddressService {

    Address findAddress(Address address, Street street);
}
