package ru.javadyori.importunet.services.address;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import ru.javadyori.importunet.models.entyties.address.Address;
import ru.javadyori.importunet.models.entyties.address.Street;
import ru.javadyori.importunet.repositories.address.AddressRepository;

import javax.persistence.EntityNotFoundException;

@RequiredArgsConstructor
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AddressServiceImpl implements AddressService {

    final AddressRepository addressRepository;
    final AddressTypeService addressTypeService;
    final StreetService streetService;


    @Override
    public Address findAddress(Address address, Street street) {
        if(address.getId() != 0) {
            return addressRepository.findById(address.getId())
                    .orElseThrow(() -> new EntityNotFoundException("Не могу найти адрес в базе!"));
        }

        return addressRepository.getAddressByCityAndStreetAndHouse(address.getCity(), street, address.getHouse())
                .orElseGet(() -> addressRepository.save(address));
    }
}
