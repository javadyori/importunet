package ru.javadyori.importunet.services.address;

import ru.javadyori.importunet.models.entyties.address.AddressType;

public interface AddressTypeService {

    AddressType getAddressType(String addressType) throws IllegalArgumentException;
}
