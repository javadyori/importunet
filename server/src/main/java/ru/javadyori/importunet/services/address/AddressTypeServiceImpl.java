package ru.javadyori.importunet.services.address;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import ru.javadyori.importunet.models.entyties.AddressName;
import ru.javadyori.importunet.models.entyties.address.AddressType;
import ru.javadyori.importunet.repositories.address.AddressTypeRepository;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AddressTypeServiceImpl implements AddressTypeService {

    final AddressTypeRepository addressTypeRepository;

    @Override
    public AddressType getAddressType(String addressType) throws IllegalArgumentException{
        AddressName addressName = AddressName.valueOf(addressType);
        return addressTypeRepository.findByName(addressName)
                .orElseGet(() -> addressTypeRepository.save(new AddressType(addressName)));
    }
}
