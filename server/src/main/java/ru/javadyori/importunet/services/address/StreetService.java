package ru.javadyori.importunet.services.address;

import ru.javadyori.importunet.models.entyties.address.Street;

public interface StreetService {

    Street getStreetByName(String name);
}
