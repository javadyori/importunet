package ru.javadyori.importunet.services.address;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import ru.javadyori.importunet.models.entyties.address.Street;
import ru.javadyori.importunet.repositories.address.StreetRepository;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StreetServiceImpl implements StreetService {

    final StreetRepository streetRepository;

    @Override
    public Street getStreetByName(String name) {

        return streetRepository.findByName(name)
                .orElseGet(() -> streetRepository.save(new Street(name)));
    }
}
