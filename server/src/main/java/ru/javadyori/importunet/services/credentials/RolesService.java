package ru.javadyori.importunet.services.credentials;

import ru.javadyori.importunet.models.entyties.user.UserRole;

import javax.management.relation.Role;
import java.util.Optional;

public interface RolesService {

    UserRole getUserRole();
    UserRole getAdminRole();
    UserRole getByName(String name) throws IllegalArgumentException;
}
