package ru.javadyori.importunet.services.credentials;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import ru.javadyori.importunet.models.Role;
import ru.javadyori.importunet.models.entyties.user.UserRole;
import ru.javadyori.importunet.repositories.users.RolesRepository;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RolesServiceImpl implements RolesService {

    final RolesRepository repository;

    @Override
    public UserRole getUserRole() {
        Optional<UserRole> byRole = repository.findByRole(Role.ROLE_USER);
                return byRole.orElseGet(() -> repository.save(new UserRole(Role.ROLE_USER)));
    }

    @Override
    public UserRole getAdminRole() {
        return repository.findByRole(Role.ROLE_ADMIN)
                .orElseGet(() -> repository.save(new UserRole(Role.ROLE_ADMIN)));
    }

    @Override
    public UserRole getByName(String name) throws IllegalArgumentException{
        Role role = Role.valueOf(name);
        return repository.findByRole(role).orElseGet(() -> repository.save(new UserRole(role)));
    }
}
