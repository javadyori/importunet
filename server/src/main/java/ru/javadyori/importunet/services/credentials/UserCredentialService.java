package ru.javadyori.importunet.services.credentials;


import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

public interface UserCredentialService {

    Optional<? extends UserDetails> findUserByUsername(String username);
    Optional<? extends UserDetails> findUserByEmail(String email);
}
