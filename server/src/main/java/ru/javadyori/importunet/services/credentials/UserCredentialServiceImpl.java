package ru.javadyori.importunet.services.credentials;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import ru.javadyori.importunet.models.credentials.UserCredentials;
import ru.javadyori.importunet.repositories.users.UserRepository;

import java.util.Optional;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class UserCredentialServiceImpl implements UserCredentialService {

    final UserRepository userRepository;

    @Override
    public Optional<? extends UserDetails> findUserByUsername(String username) {
        return userRepository.findByUsername(username).map(userInfo -> new UserCredentials(userInfo));
    }

    @Override
    public Optional<? extends UserDetails> findUserByEmail(String email) {
        return userRepository.findByEmail(email).map(userInfo -> new UserCredentials(userInfo));
    }
}
