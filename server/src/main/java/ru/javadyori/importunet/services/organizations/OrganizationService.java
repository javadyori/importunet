package ru.javadyori.importunet.services.organizations;

import ru.javadyori.importunet.models.dtos.orgs.CreateOrganizationDto;
import ru.javadyori.importunet.models.dtos.orgs.OrganizationDto;

import java.util.List;
import java.util.Optional;

public interface OrganizationService {

    OrganizationDto addOrganization(CreateOrganizationDto createOrganizationDto) throws IllegalArgumentException;
    OrganizationDto updateOrganization(CreateOrganizationDto createOrganizationDto) throws IllegalArgumentException;
    Optional<OrganizationDto> getOrganizationByName(String organizationName);
    List<OrganizationDto> getAllOrganizations();
    Optional<OrganizationDto> getOrganizationById(long id);
    Optional<OrganizationDto> getOrganizationByInnNumber(String innNumber);
}
