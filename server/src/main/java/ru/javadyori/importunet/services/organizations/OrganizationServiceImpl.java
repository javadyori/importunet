package ru.javadyori.importunet.services.organizations;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import ru.javadyori.importunet.mappers.address.AddressMapper;
import ru.javadyori.importunet.models.dtos.orgs.CreateOrganizationDto;
import ru.javadyori.importunet.models.dtos.orgs.OrganizationDto;
import ru.javadyori.importunet.mappers.orgs.OrganizationMapper;
import ru.javadyori.importunet.models.entyties.address.Address;
import ru.javadyori.importunet.models.entyties.address.AddressType;
import ru.javadyori.importunet.models.entyties.address.Street;
import ru.javadyori.importunet.models.entyties.orgs.*;
import ru.javadyori.importunet.repositories.orgs.OrganizationRepository;
import ru.javadyori.importunet.services.address.AddressService;
import ru.javadyori.importunet.services.address.AddressTypeService;
import ru.javadyori.importunet.services.address.StreetService;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrganizationServiceImpl implements OrganizationService {

    final OrganizationRepository organizationRepository;
    final AddressService addressService;
    final AddressMapper addressMapper;
    final OrganizationMapper mapper;
    final OrganizationTypeService organizationTypeService;
    final OwnershipTypeService ownershipTypeService;
    final StreetService streetService;
    final AddressTypeService addressTypeService;



    @Override
    //TODO: Refactor
    public OrganizationDto addOrganization(CreateOrganizationDto createOrganizationDto) throws IllegalArgumentException{

        if(createOrganizationDto.id() != 0) {
            Optional<Organization> byId = organizationRepository.findById(createOrganizationDto.id());
            if (byId.isPresent()) {
                return updateOrganization(createOrganizationDto);
            } else throw new IllegalArgumentException("Не могу найти организацию с таким id");

        }

        Optional<Organization> byInnNumber = organizationRepository.findByInnNumber(createOrganizationDto.innNumber());

        if(byInnNumber.isPresent()) {
            throw new IllegalArgumentException("Организация с таким ИНН уже зарегистрирована");
        }

        return getOrganizationDto(createOrganizationDto);
    }

    @Override
    public OrganizationDto updateOrganization(CreateOrganizationDto createOrganizationDto) throws IllegalArgumentException {
        if(createOrganizationDto.id() == 0) throw new IllegalArgumentException("Параметр id отсутствует");

        return getOrganizationDto(createOrganizationDto);
    }

    public OrganizationDto getOrganizationDto(CreateOrganizationDto dto) {
        Organization organization = mapper.toEntity(dto);

        if (dto.address() != null) {
            Street street = streetService.getStreetByName(dto.address().street());
            AddressType addressType = addressTypeService.getAddressType(dto.address().addressType());
            Address address = addressMapper.toEntity(dto.address());
            address = addressService.findAddress(address, street);
            address.setOrganization(organization);
            address.setStreet(street);
            street.getAddresses().add(address);
            addressType.getAddresses().add(address);
            address.setAddressType(addressType);

            organization.setAddress(address);
        }

        if (dto.organizationType() != null) {
            OrganizationType organizationType = organizationTypeService.findByName(dto.organizationType());
            organizationType.getOrganizations().removeIf(el -> el.equals(organization));
            organizationType.getOrganizations().add(organization);

            organization.setOrganizationType(organizationType);
        }

        if (dto.ownershipType() != null) {
            OwnershipType ownershipType = ownershipTypeService.findByNameAndFnsCode(dto.ownershipType().name(), dto.ownershipType().fnsCode());
            ownershipType.getOrganizations().removeIf(el -> el.equals(organization));
            ownershipType.getOrganizations().add(organization);

            organization.setOwnershipType(ownershipType);
        }

        return mapper.toDto(organizationRepository.save(organization));
    }

    @Override
    public Optional<OrganizationDto> getOrganizationByName(String organizationName) {
        return Optional.empty();
    }

    @Override
    public List<OrganizationDto> getAllOrganizations() {
        return null;
    }

    @Override
    public Optional<OrganizationDto> getOrganizationById(long id) {
        return Optional.empty();
    }

    @Override
    public Optional<OrganizationDto> getOrganizationByInnNumber(String innNumeber) {
        return Optional.empty();
    }
}
