package ru.javadyori.importunet.services.organizations;

import ru.javadyori.importunet.models.entyties.orgs.OrganizationType;

public interface OrganizationTypeService {

    OrganizationType findByName(String name) throws IllegalArgumentException;
}
