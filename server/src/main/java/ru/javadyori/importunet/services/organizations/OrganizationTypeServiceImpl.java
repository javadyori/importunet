package ru.javadyori.importunet.services.organizations;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import ru.javadyori.importunet.models.entyties.orgs.OrganizationType;
import ru.javadyori.importunet.models.entyties.orgs.OrganizationTypeName;
import ru.javadyori.importunet.repositories.orgs.OrganizationTypeRepository;

@Service
@RequiredArgsConstructor
@FieldDefaults(level= AccessLevel.PRIVATE)
public class OrganizationTypeServiceImpl implements OrganizationTypeService {

    final OrganizationTypeRepository repository;

    @Override
    public OrganizationType findByName(String name) throws IllegalArgumentException{
        OrganizationTypeName organizationTypeName = OrganizationTypeName.valueOf(name);
        return repository.findByName(organizationTypeName)
                .orElseGet(() -> repository.save(new OrganizationType(organizationTypeName)));
    }
}
