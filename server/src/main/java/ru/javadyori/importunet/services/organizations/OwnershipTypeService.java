package ru.javadyori.importunet.services.organizations;

import ru.javadyori.importunet.models.entyties.orgs.OwnershipType;

public interface OwnershipTypeService {

    OwnershipType findByNameAndFnsCode(String name, String fnsCode);
}
