package ru.javadyori.importunet.services.organizations;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import ru.javadyori.importunet.models.entyties.orgs.OwnershipType;
import ru.javadyori.importunet.repositories.orgs.OwnershipTypeRepository;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OwnershipTypeServiceImpl implements OwnershipTypeService {

    final OwnershipTypeRepository ownershipTypeRepository;
    @Override
    public OwnershipType findByNameAndFnsCode(String name, String fnsCode) {

        Optional<OwnershipType> ownershipType = ownershipTypeRepository.findByNameAndFnsCode(name, fnsCode);
                return ownershipType.orElseGet(() -> ownershipTypeRepository.save(new OwnershipType()
                        .setName(name)
                        .setFnsCode(fnsCode)));

    }
}
