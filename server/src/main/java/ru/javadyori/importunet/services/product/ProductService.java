package ru.javadyori.importunet.services.product;


import ru.javadyori.importunet.models.dtos.product.CreateProductDto;
import ru.javadyori.importunet.models.dtos.product.ProductDto;

import javax.persistence.EntityNotFoundException;

public interface ProductService {

    ProductDto createProduct(CreateProductDto dto) throws EntityNotFoundException, IllegalArgumentException;
}
