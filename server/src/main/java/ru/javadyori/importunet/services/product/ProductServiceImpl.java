package ru.javadyori.importunet.services.product;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import ru.javadyori.importunet.mappers.product.ProductMapper;
import ru.javadyori.importunet.models.dtos.product.CreateProductDto;
import ru.javadyori.importunet.models.dtos.product.ProductDto;
import ru.javadyori.importunet.models.entyties.orgs.Organization;
import ru.javadyori.importunet.models.entyties.product.Product;
import ru.javadyori.importunet.repositories.orgs.OrganizationRepository;
import ru.javadyori.importunet.repositories.product.ProductRepository;

import javax.persistence.EntityNotFoundException;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductServiceImpl implements ProductService {

    final ProductMapper productMapper;
    final ProductRepository productRepository;
    final OrganizationRepository organizationRepository;

    @Override
    public ProductDto createProduct(CreateProductDto dto) throws EntityNotFoundException, IllegalArgumentException {

        if (dto.organizationId() == 0) throw new IllegalStateException("Не указан id организации");

        Organization organization = organizationRepository.findById(dto.organizationId())
                .orElseThrow(() -> new EntityNotFoundException("Не могу найли организация с id: " + dto.organizationId()));

        Product product = productMapper.toEntity(dto);
        product.setOrganization(organization);

        return productMapper.toDto(productRepository.save(product));
    }
}
