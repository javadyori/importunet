package ru.javadyori.importunet.services.users;

import ru.javadyori.importunet.models.dtos.orgs.LoginDto;
import ru.javadyori.importunet.models.dtos.user.UpdateUserInfoDto;
import ru.javadyori.importunet.models.dtos.user.UserInfoDto;

import java.util.List;
import java.util.Optional;

public interface UserService {

    UserInfoDto createUser(LoginDto dto);
    UserInfoDto updateUser(UpdateUserInfoDto dto) throws Exception;
    List<UserInfoDto> getAllUsers();
    List<UserInfoDto> getAllUsersByName(String name);
    List<UserInfoDto> getAllUsersBySecondName(String secongName);
    Optional<UserInfoDto> getUserBuId(long id);
    Optional<UserInfoDto> getUserByUsername(String username);
}
