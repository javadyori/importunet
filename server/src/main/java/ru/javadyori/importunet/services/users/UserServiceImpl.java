package ru.javadyori.importunet.services.users;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.javadyori.importunet.mappers.user.UserMapper;
import ru.javadyori.importunet.models.dtos.orgs.LoginDto;
import ru.javadyori.importunet.models.dtos.user.UpdateUserInfoDto;
import ru.javadyori.importunet.models.dtos.user.UserInfoDto;
import ru.javadyori.importunet.models.entyties.user.UserInfo;
import ru.javadyori.importunet.repositories.users.UserRepository;
import ru.javadyori.importunet.services.credentials.RolesService;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Slf4j
public class UserServiceImpl implements UserService {

    final UserRepository userRepository;
    final RolesService rolesService;
    final UserMapper userMapper;

    @Override
    public UserInfoDto createUser(LoginDto dto) {
        log.info("Создаю нового пользователя");
        UserInfo userInfo = new UserInfo().setUsername(dto.getUsername())
                .setPassword(dto.getPassword())
                .setEmail(dto.getEmail())
                .setRoles(Set.of(rolesService.getUserRole()));

        return userMapper.toDto(userRepository.save(userInfo));
    }

    @Override
    public UserInfoDto updateUser(UpdateUserInfoDto dto) throws Exception {

        log.info("Обновляю данныю пользователя");
        return userRepository.findByUsername(dto.username())
                .map(user -> {
                    var entity = userMapper.toEntity(dto);
                    entity.setUserId(user.getUserId());
                    entity.setEmail(user.getEmail());
                    entity.setUsername(user.getUsername());
                    entity.setPassword(user.getPassword());
                    entity.setCreationDate(user.getCreationDate());
                    return entity;
                })
                .map(userRepository::save)
                .map(userMapper::toDto)
                .orElseThrow(() -> new UsernameNotFoundException("Пользователь не найден!"));
    }

    @Override
    public List<UserInfoDto> getAllUsers() {
        return userRepository.findAll().stream().map(userMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<UserInfoDto> getAllUsersByName(String name) {
        return userRepository.findAllByFirstName(name).stream().map(userMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<UserInfoDto> getAllUsersBySecondName(String secondName) {
        return userRepository.findAllBySecondName(secondName).stream().map(userMapper::toDto).collect(Collectors.toList());
    }


    @Override
    public Optional<UserInfoDto> getUserBuId(long id) {
        return userRepository.findById(id).map(userMapper::toDto);
    }

    @Override
    public Optional<UserInfoDto> getUserByUsername(String username) {
        return userRepository.findByUsername(username).map(userMapper::toDto);
    }
}
