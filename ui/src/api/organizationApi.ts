import {Org} from "../components/DataProvider";
import axios from "axios";

interface OrgResponse {
    readonly id: number
    readonly name: string
    readonly phone: string
    readonly website: string
    readonly innNumber: string
    readonly description: string
    readonly ownershipType: object
    readonly organizationType: string
    readonly address: object
    readonly creationDate: Date
    readonly lastUpdate: Date
}

export const organizationApi = {
    create(org: Org, onSuccess: (user: Org) => void, onError: (reason: any) => void = console.error) {
        axios.post("/api/rest/org/", {
            name: org.name,
            phone: org.telephone,
            website: org.website,
            innNumber: org.inn,
            ownershipType: null,
            organizationType: null,
            address: null,
            description: org.description
        }).then(response => {
            if (response.status === 200) {
                onSuccess(mapOrgResponseToOrg(response.data))
            } else {
                onError(response)
            }
        }).catch(message => {
            if (message.response.status === 401) {
                location.href = "/index.html"
            } else {
                onError(message)
            }
        })
    }
}

const mapOrgResponseToOrg = (rs: OrgResponse): Org => ({
    id: rs.id,
    name: rs.name,
    description: rs.description,
    telephone: rs.phone,
    inn: rs.innNumber,
    website: rs.website
})
