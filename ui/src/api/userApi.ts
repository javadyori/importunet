import {User} from "../components/DataProvider";
import axios from "axios";

interface UserResponse {
    readonly userId: number
    readonly username: string
    readonly email: string
    readonly phone?: string | null
    readonly firstName?: string | null
    readonly secondName?: string | null
    readonly middleName?: string | null
    readonly birthdayDate?: Date | null
}

export const userApi = {
    getUser(onSuccess: (user: User) => void, onError: (reason: any) => void = console.error) {
        axios.get("/api/rest/user/whoami")
            .then(response => {
                if (response.status === 200) {
                    onSuccess(mapUserResponseToUser(response.data))
                } else {
                    onError(response)
                }
            }).catch(message => {
                if (message.response.status === 401) {
                    location.href = "/index.html"
                } else {
                    onError(message)
                }
            })
    }
}

const mapUserResponseToUser = (rs: UserResponse): User => ({
    id: rs.userId,
    email: rs.email,
    firstName: rs.firstName,
    secondName: rs.secondName,
    patronymic: rs.middleName,
    birthDate: rs.birthdayDate,
    telephone: rs.phone
})