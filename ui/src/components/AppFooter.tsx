import React from 'react'
import {Center, Footer} from "@mantine/core";

export const AppFooter: React.FC = () => {
    return <Footer height={64}>
        <Center style={{width: "100%", height: 64}}>Сделано командой Javaдёры 2022</Center>
    </Footer>
}