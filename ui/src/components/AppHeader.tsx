import React from 'react'
import {ActionIcon, Button, ColorScheme, Grid, Group, Header, Image} from "@mantine/core";
import {Moon, Sun} from "tabler-icons-react";
import {AppState, HomeState} from "../utils/appState";
import {GRADIENT} from "../utils/style";

import logo from "../images/logo.svg"
import logoW from "../images/logo_white_large.png"

const NO_MARGIN: React.CSSProperties = {margin: 0}

export interface AppHeaderProps {
    openLogin?(): void

    openSignUp?(): void

    readonly colorScheme: ColorScheme

    toggleColorScheme(value?: ColorScheme): void

    appState: AppState | HomeState
}

const contains = (...states: (AppState | HomeState)[]) => (state: AppState | HomeState): boolean => states.indexOf(state) > -1

export const AppHeader: React.FC<AppHeaderProps> = ({
                                                        openLogin,
                                                        toggleColorScheme,
                                                        colorScheme,
                                                        openSignUp,
                                                        appState
                                                    }) => {
    return <Header height={64} fixed>
        <Grid grow style={NO_MARGIN} align="center">
            <Grid.Col span={4}>
                <Group position="left">
                    <Image
                        src={colorScheme === "light" ? logo : logoW}
                        alt="Importu.net"
                        height="34px"
                        fit="contain"
                        mt="6px"
                        ml="15px"
                    />
                </Group>
            </Grid.Col>
            <Grid.Col span={4}>
                <Group position="right" spacing="sm">
                    {contains("PREVIEW", "SIGNUP")(appState) &&
                        <Button variant="outline" onClick={openLogin}>Войти</Button>}

                    {contains("PREVIEW", "SIGNUP")(appState) &&
                        <Button onClick={openSignUp} variant="gradient" gradient={GRADIENT}>Регистрация</Button>}

                    <ActionIcon
                        size="lg"
                        variant="light"
                        onClick={() => toggleColorScheme()}
                    >
                        {colorScheme === "light" ? <Moon/> : <Sun/>}
                    </ActionIcon>
                </Group>
            </Grid.Col>
        </Grid>
    </Header>
}