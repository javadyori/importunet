import React from "react";

export interface Root {
    readonly user: User | null
    readonly orgs: Org[]
}

export interface User {
    readonly id: number
    readonly email: string
    readonly firstName?: string | null
    readonly secondName?: string | null
    readonly patronymic?: string | null
    readonly birthDate?: Date | null
    readonly telephone?: string | null
    readonly organizationId?: number | null
}

export interface Org {
    readonly id?: number | null
    readonly name?: string | null
    readonly description?: string | null
    readonly telephone?: string | null
    readonly inn?: string | null
    readonly website?: string | null
}


export const defaultData: Root = {
    user: null,
    orgs: []
}

type RootContext = Root & { update(root: Root): void }

export const Data = React.createContext<RootContext>({
    ...defaultData, update: () => {}
})