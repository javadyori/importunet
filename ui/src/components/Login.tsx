import React, {useState} from 'react'
import {Button, Group, Modal, Space, Text, TextInput} from "@mantine/core";
import {ModalHook} from '../utils/modal';
import {Lock, Login as LoginImage, Mail} from 'tabler-icons-react';
import {useForm, zodResolver} from '@mantine/form';
import {z} from 'zod';
import axios from "axios";
import {GRADIENT} from "../utils/style";

export interface LoginProps {
    readonly modal: ModalHook
}

const schema = z.object({
    email: z.string().email("Некоректный email"),
    password: z.string().min(1, "Введите пароль")
})

interface LoginForm {
    readonly email: string
    readonly password: string
}

export const Login: React.FC<LoginProps> = ({modal}) => {
    const [error, setError] = useState<string>("")

    const form = useForm<LoginForm>({
        initialValues: {
            email: "",
            password: ""
        },

        schema: zodResolver(schema)
    })

    const submit = ({email, password}: LoginForm) => {
        setError("")
        axios.get("/api/rest/auth/signIn", {
            headers: {
                "Authorization": `Basic ${btoa(`${email}:${password}`)}`
            },
            maxRedirects: 0
        }).then(response => {
            if (response.status === 200) {
                location.href = response.request.responseURL
            } else {
                setError("Неверный логин или пароль")
            }
        }).catch(() => setError("Неудачная попытка авторизации"))
    }

    return <Modal
        opened={modal.opened}
        onClose={() => {
            modal.close();
            form.reset()
        }}
        centered
        title={<Text size='lg' weight={600}>Авторизация</Text>}
    >
        <form onSubmit={form.onSubmit(submit)}>
            <TextInput
                label="Адрес электронной почты"
                placeholder="example@yandex.ru"
                icon={<Mail/>}
                type="email"
                {...form.getInputProps("email")}
            >

            </TextInput>

            <Space h="sm"/>

            <TextInput
                label="Пароль"
                placeholder="Ваш пароль"
                icon={<Lock/>}
                type="password"
                {...form.getInputProps("password")}
            >

            </TextInput>

            {error !== "" && <>
                <Space h="sm"/>
                <Text color="red">{error}</Text>
            </>}

            <Space h="lg"/>

            <Group position="right">
                <Button type="submit" variant="gradient" gradient={GRADIENT}>
                    <LoginImage/> Войти
                </Button>
            </Group>
        </form>
    </Modal>
}