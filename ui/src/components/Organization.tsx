import React, {useContext, useState} from "react";
import {Data, Org} from "./DataProvider";
import {Alert, Button, Center, Group, Paper, Space, Stack, Text, Textarea, TextInput, Title} from "@mantine/core";
import {AlertCircle, Phone, Plus, World} from "tabler-icons-react";
import {GRADIENT} from "../utils/style";
import {useForm} from "@mantine/form";
import {organizationApi} from "../api/organizationApi";

const WIDTH_FULL: React.CSSProperties = {width: "100%"}

export const Organization: React.FC = () => {
    const {user, orgs, update} = useContext(Data)
    const [org, setOrg] = useState<{ org: Org, isEdit: boolean }>({org: {}, isEdit: false})
    const [error, setError] = useState<string>("")

    const form = useForm<Org>({
        initialValues: org.org,
    })

    const saveOrg = (org: Org) => {
        setError("")
        organizationApi.create(org,
            org => {
                setOrg({org, isEdit: false})
                update({
                    orgs: [...orgs, org],
                    user: user === null ? null : {...user, organizationId: org.id}
                })
            },
            e => {
                setError("Ошибка сохранения")
            })
    }

    if (!user?.firstName) { //Нет прав
        return <Center style={WIDTH_FULL}>
            <Alert icon={<AlertCircle size={16}/>} title="Аккаунт не верифицирован!" color="yellow" p="xl">
                Вы не можете добавлить организацию, пока не заполнены<br/>
                дынные пользователя и не пройдена верификация
            </Alert>
        </Center>
    } else if (!user?.organizationId && !org.isEdit) { //Есть права можно создать организацию
        return <Center style={WIDTH_FULL}>
            <Paper shadow="xs" radius="xs" p="md" withBorder>
                <Stack spacing="xl">
                    <Text>
                        Пока что вы не привязаны к организации!<br/>
                        Выможете создать свою организацию, или дождаться <br/>
                        пока вас добавят к организации.
                    </Text>
                    <Button
                        size="lg"
                        onClick={() => setOrg({org: {}, isEdit: true})}
                        variant="gradient"
                        gradient={GRADIENT}
                    >
                        <Plus/> Создать организацию
                    </Button>
                </Stack>
            </Paper>
        </Center>
    } else if (org.isEdit) { //Создание или редактирование организации
        return <Center style={WIDTH_FULL}>
            <Paper shadow="xs" radius="xs" p="md" withBorder style={{width: 500}}>
                <form onSubmit={form.onSubmit(saveOrg)}>
                    <Stack>
                        <TextInput
                            label="Наименование"
                            type="text"
                            {...form.getInputProps("name")}
                        />
                        <TextInput
                            label="ИНН"
                            type="text"
                            {...form.getInputProps("inn")}
                        />
                        <TextInput
                            label="Телефон"
                            type="tel"
                            {...form.getInputProps("telephone")}
                        />
                        <TextInput
                            label="Сайт"
                            type="url"
                            {...form.getInputProps("website")}
                        />
                        <Textarea
                            label="Описание"
                            rows={6}
                            {...form.getInputProps("description")}
                        />

                        {error !== "" && <>
                            <Space h="sm"/>
                            <Text color="red">{error}</Text>
                        </>}

                        <Group position="right">
                            <Button size="lg" variant="outline"
                                    onClick={() => setOrg({org: {}, isEdit: false})}>Отменить</Button>
                            <Button size="lg" variant="gradient" gradient={GRADIENT} type="submit">Сохранить</Button>
                        </Group>
                    </Stack>
                </form>
            </Paper>
        </Center>

    } else { //Просмотр организации
        if (user.organizationId !== null && org.org.name === undefined) {
            const filtered = orgs.filter(value => value.id === user.organizationId)
            console.log(filtered)
            if (filtered.length > 0) {
                setOrg({...org, org: filtered[0]})
            }
        }

        return <Center style={WIDTH_FULL}>
            <Paper shadow="xs" radius="xs" p="md" withBorder style={{width: 700}}>
                <Stack>
                    <Title order={2}>{org.org.name}</Title>
                    <Text>ИНН: {org.org.inn}</Text>
                    <Text><Phone/> Телефон: {org.org.telephone}</Text>
                    <Text><World/> Сайт: {org.org.website}</Text>
                    <Space/>
                    <Text>{org.org.description}</Text>
                </Stack>
            </Paper>
        </Center>
    }
}