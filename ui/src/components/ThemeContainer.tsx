import React from 'react'
import {ColorScheme, ColorSchemeProvider, MantineProvider} from "@mantine/core";

interface ThemeContainerProps {
    readonly children: React.ReactNode
    readonly colorScheme: ColorScheme

    toggleColorScheme(value?: ColorScheme): void
}

export const ThemeContainer: React.FC<ThemeContainerProps> = ({children, colorScheme, toggleColorScheme}) => {
    return <ColorSchemeProvider colorScheme={colorScheme} toggleColorScheme={toggleColorScheme}>
        <MantineProvider theme={{colorScheme, primaryColor: 'pink'}} withGlobalStyles withNormalizeCSS>
            {children}
        </MantineProvider>
    </ColorSchemeProvider>
}