import React, {useEffect, useState} from 'react'
import ReactDOM from 'react-dom'
import {ThemeContainer} from "../components/ThemeContainer";
import {AppShell, ColorScheme} from "@mantine/core";
import {AppHeader} from "../components/AppHeader";
import {useLocalStorage} from "@mantine/hooks";
import {HomeState} from "../utils/appState";
import {Data, defaultData, Root} from "../components/DataProvider";
import {userApi} from "../api/userApi";
import {Organization} from "../components/Organization";

import "./index.css"

const App = () => {
    const [appState] = useState<HomeState>("VIEW_ORG")

    const [root, update] = useState<Root>(defaultData)

    useEffect(() => {
        root.user === null && userApi.getUser(user => update({...root, user}))
    })

    const [colorScheme, setColorScheme] = useLocalStorage<ColorScheme>({
        key: 'mantine-color-scheme',
        defaultValue: 'light',
        getInitialValueInEffect: true,
    })

    const toggleColorScheme = (value?: ColorScheme) =>
        setColorScheme(value || (colorScheme === 'dark' ? 'light' : 'dark'))

    return <ThemeContainer colorScheme={colorScheme} toggleColorScheme={toggleColorScheme}>
        <Data.Provider value={{...root, update}}>
            <AppShell
                padding="md"
                header={<AppHeader
                    appState={appState}
                    toggleColorScheme={toggleColorScheme}
                    colorScheme={colorScheme}
                />}
                style={{paddingTop: 64}}
            >
                <Organization/>
            </AppShell>
        </Data.Provider>
    </ThemeContainer>
}

ReactDOM.render(<App/>, document.getElementById("app"))