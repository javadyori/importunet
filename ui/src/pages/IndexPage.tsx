import React, {useState} from 'react'
import ReactDOM from 'react-dom'
import {ThemeContainer} from "../components/ThemeContainer";
import {AppShell, Center, ColorScheme, Image, Stack, Text, Title} from "@mantine/core";
import {AppHeader} from "../components/AppHeader";
import {useModal} from '../utils/modal';
import {Login} from '../components/Login';
import {useLocalStorage} from "@mantine/hooks";
import {AppState} from "../utils/appState";
import {SignUp} from "../signup/SignUp";
import {AppFooter} from "../components/AppFooter";

import background from "../images/image.jpg"
import "./index.css"
import logo from "../images/logo.svg";
import logoW from "../images/logo_white_large.png";

const App = () => {
    const loginModal = useModal()

    const [appState, setAppState] = useState<AppState>("PREVIEW")

    const [colorScheme, setColorScheme] = useLocalStorage<ColorScheme>({
        key: 'mantine-color-scheme',
        defaultValue: 'light',
        getInitialValueInEffect: true,
    })

    const toggleColorScheme = (value?: ColorScheme) =>
        setColorScheme(value || (colorScheme === 'dark' ? 'light' : 'dark'))

    const Body = () => {
        switch (appState) {
            case "SIGNUP":
                return <SignUp toPreview={() => setAppState("PREVIEW")} openLogin={loginModal.open}/>
            default:
                return <>
                    <Center style={{position: "absolute", top: "45vh", zIndex: 5, margin: "auto", width: "100%"}}>
                        <Stack>
                            <Image
                                src={colorScheme === "light" ? logo : logoW}
                                alt="Importu.net"
                                fit="contain"
                                width={785}
                            />
                            <Title order={1}>
                                <Text color="#ff8888" inherit component="span">
                                    Построим разрушенные цепочки в своей стране
                                </Text>
                            </Title>
                        </Stack>
                    </Center>
                    <Image
                        src={background}
                        alt="Фон"
                        width="100%"
                        height="calc(100vh - 64px)"
                        style={{filter: "grayscale(75%) brightness(100%)", opacity: 0.65}}
                    />
                </>
        }
    }

    return <ThemeContainer colorScheme={colorScheme} toggleColorScheme={toggleColorScheme}>
        <Login modal={loginModal}/>

        <AppShell
            header={<AppHeader
                appState={appState}
                openLogin={loginModal.open}
                openSignUp={() => setAppState("SIGNUP")}
                toggleColorScheme={toggleColorScheme}
                colorScheme={colorScheme}
            />}
            footer={<AppFooter/>}
            style={{paddingTop: 64}}
        >
            <Body/>
        </AppShell>
    </ThemeContainer>
}

ReactDOM.render(<App/>, document.getElementById("app"))