import React, {useState} from "react"
import {Stepper} from "@mantine/core";
import {LoginPassStep} from "./steps/LoginPassStep";

import "./step.css"
import {AddInfoStep} from "./steps/AddInfoStep";
import {VerifyStep} from "./steps/VerifyStep";

interface SignUpProps {
    toPreview(): void
    openLogin(): void
}

export const SignUp: React.FC<SignUpProps> = ({toPreview, openLogin}) => {
    const [active, setActive] = useState<number>(0)
    const [needVerify, setNeedVerify] = useState<boolean>(false)

    return <Stepper active={active} orientation="vertical" style={{display: "flex"}} m="xl">
        <Stepper.Step
            label="Заполните учётные данные"
            description="Для входа в личный кабинет"
        >
            <LoginPassStep next={() => setActive(1)} skip={toPreview}/>
        </Stepper.Step>

        <Stepper.Step
            label="Дополнительная информация"
            description="Для последующей верификации"
        >
            <AddInfoStep
                next={() => {
                    setNeedVerify(true);
                    setActive(2)
                }}
                skip={() => {
                    setNeedVerify(false);
                    setActive(2)
                }}
            />
        </Stepper.Step>

        <Stepper.Step label="Верификация">
            <VerifyStep
                needVerify={needVerify}
                next={openLogin}
                skip={toPreview}/>
        </Stepper.Step>
    </Stepper>
}