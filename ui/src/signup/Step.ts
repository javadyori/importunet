export type StepProps<T = {}> = T & {
    next(): void
    skip(): void
}