import 'dayjs/locale/ru';
import React, {useState} from "react"
import {StepProps} from "../Step";
import {Button, Center, Group, Stack, Text, TextInput} from "@mantine/core";
import {Calendar} from "tabler-icons-react";
import {useForm} from "@mantine/form";
import {DatePicker} from "@mantine/dates";
import {GRADIENT} from "../../utils/style";
import axios from "axios";

interface AddInfoStepForm {
    readonly firstName: string
    readonly lastName: string
    readonly patronymic: string
    readonly birthDay: Date | null
    readonly telephone: string
}

export const AddInfoStep: React.FC<StepProps> = ({next, skip}) => {
    const [error, setError] = useState<string>("")

    const form = useForm<AddInfoStepForm>({
        initialValues: {
            firstName: "",
            lastName: "",
            patronymic: "",
            birthDay: null,
            telephone: ""
        }
    })

    const submit = (form: AddInfoStepForm) => {
        setError("")
        axios.put("/api/rest/user", {
            phone: form.telephone,
            firstName: form.firstName,
            secondName: form.lastName,
            middleName: form.patronymic,
            birthdayDate: form.birthDay
        }).then(response => {
            if (response.status == 200) {
                next()
            } else {
                setError(response.data)
            }
        }).catch(message => {
            if (message.response.status === 401) {
                location.href = "/index.html"
            } else {
                setError("Ошибка запроса")
            }
        })
    }

    return <Center>
        <form onSubmit={form.onSubmit(submit)}>
            <Stack>
                <Text color="gray">
                    Заполните дополнительную информацию о себе.<br/>
                    Она необходима для последующей верификации.<br/>
                    Без верификации нельзя работать с ЮЛ!
                </Text>

                <TextInput
                    required
                    label="Фамилия"
                    placeholder="Иванов"
                    type="text"
                    {...form.getInputProps("lastName")}
                />

                <TextInput
                    required
                    label="Имя"
                    placeholder="Иван"
                    type="text"
                    {...form.getInputProps("firstName")}
                />

                <TextInput
                    label="Отчество"
                    placeholder="Иванович"
                    type="text"
                    {...form.getInputProps("patronymic")}
                />

                <DatePicker
                    required
                    locale="ru"
                    label="Дата рождения"
                    placeholder="Дата рождения"
                    icon={<Calendar/>}
                    {...form.getInputProps("birthDay")}
                />

                <TextInput
                    required
                    label="Телефон"
                    placeholder="+7(999)999-99-99"
                    type="tel"
                    {...form.getInputProps("telephone")}
                />

                {error !== "" && <Text color="red">{error}</Text>}

                <Group>
                    <Button variant="outline" onClick={skip}>Пропустить</Button>
                    <Button type="submit" variant="gradient" gradient={GRADIENT}>Сохранить</Button>
                </Group>
            </Stack>
        </form>
    </Center>
}