import {Button, Center, Group, Stack, Text, TextInput} from "@mantine/core";
import React, {useState} from "react"
import {StepProps} from "../Step";
import {Lock, Mail} from "tabler-icons-react";
import {z} from "zod";
import {useForm} from "@mantine/form";
import axios from "axios";
import {GRADIENT} from "../../utils/style";

interface SignUpForm {
    readonly email: string
    readonly password: string
    readonly password2: string
}

export const LoginPassStep: React.FC<StepProps> = ({next, skip}) => {
    const [error, setError] = useState<string>("")

    const form = useForm<SignUpForm>({
        initialValues: {
            email: "",
            password: "",
            password2: ""
        },
        validate: {
            email: value => {
                try {
                    z.string().email("Некоректный email").parse(value)
                    return null
                } catch (e) {
                    return "Некоректный email"
                }
            },
            password: value => value.length > 7 && /[0-9]/.test(value) && /[a-z]/.test(value) && /[A-Z]/.test(value)
                ? null
                : "Пароль должен быть длиной минимум 8 символов \nи содержать 1 строчную латинскую букву, 1 заглавную и 1 цифру",
            password2: (value, values) => value !== values.password ? "Пароли не совпадают" : null
        }
    })

    const signup = ({email, password}: SignUpForm) => {
        setError("")
        axios.post("/api/rest/auth/signUp", {
            email,
            password,
            username: email
        }).then(response => {
            if (response.status === 200) {
                next()
            } else {
                setError(response.data)
            }
        }).catch(() => setError("Ошибка регистрации"))
    }
    return <Center>
        <form onSubmit={form.onSubmit(signup)}>
            <Stack>
                <TextInput
                    label="Адрес электронной почты"
                    placeholder="example@yandex.ru"
                    icon={<Mail/>}
                    type="email"
                    {...form.getInputProps("email")}
                />

                <TextInput
                    label="Пароль"
                    placeholder="Ваш пароль"
                    icon={<Lock/>}
                    type="password"
                    {...form.getInputProps("password")}
                />

                <TextInput
                    label="Повторите пароль"
                    placeholder="Ещё раз ваш пароль"
                    icon={<Lock/>}
                    type="password"
                    {...form.getInputProps("password2")}
                />

                {error !== "" && <Text color="red">{error}</Text>}

                <Group>
                    <Button variant="outline" onClick={skip}>Я передумал</Button>
                    <Button type="submit" variant="gradient" gradient={GRADIENT}>Зарегистрироваться</Button>
                </Group>
            </Stack>
        </form>
    </Center>
}