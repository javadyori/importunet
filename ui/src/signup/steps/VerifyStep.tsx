import React from "react"
import {StepProps} from "../Step";
import {Button, Center, Group, Stack, Text} from "@mantine/core";
import {GRADIENT} from "../../utils/style";

export const VerifyStep: React.FC<StepProps<{ needVerify: boolean }>> = ({next, skip, needVerify}) => {
    return <Center>
        <Stack>
            {needVerify
                ? <Text color="gray">
                    Началась верификация пользователя.<br/>
                    Это займёт какое-то время.<br/>
                    Но вы уже можете пользоваться личным кабинетом.
                </Text>
                : <Text color="gray">
                    Верификация не началсь.<br/>
                    Возможности личного кабинета ограничены.<br/>
                    Чтобы заполнить данные войдите в личный кабинет.
                </Text>
            }

            <Group>
                <Button variant="outline" onClick={skip}>На главную</Button>
                <Button onClick={next} variant="gradient" gradient={GRADIENT}>Войти</Button>
            </Group>
        </Stack>
    </Center>
}