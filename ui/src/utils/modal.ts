import {useState} from "react"

export interface ModalHook {
    readonly opened: boolean

    open(): void

    close(): void
}

export const useModal = (): ModalHook => {
    const [opened, update] = useState(false)

    return {
        opened,
        open: () => update(true),
        close: () => update(false)
    }
}
